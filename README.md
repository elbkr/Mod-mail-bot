# Discord modmail bot

## How to make it work?

- Clone or download this repository
- Enter code editor
- Run `npm install` on the console
- Go to config.json and edit:
> `token` with your bot's token
> 
> `prefix` with the prefix of your choice
> 
> `ServerID` with the id of your server
> 
- Run the bot and all done!

- Run the setup command!
